#include <bits/stdc++.h>
#include <random>
#include<iostream>
#include<fstream>
#include<string>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <chrono>
#include<thread>
using namespace std;

struct AdjListNode
{
    int dest;
    int weight;
    struct AdjListNode* next;
};
  
struct AdjList
{
   struct AdjListNode *head; 
};
  
struct Graph
{
    int V;
    struct AdjList* array;
};
  
struct AdjListNode* newAdjListNode(
                   int dest, int weight)
{
    struct AdjListNode* newNode =
            (struct AdjListNode*) 
      malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->weight = weight;
    newNode->next = NULL;
    return newNode;
}
  
struct Graph* createGraph(int V)
{
    struct Graph* graph = (struct Graph*) 
            malloc(sizeof(struct Graph));
    graph->V = V;
  

    graph->array = (struct AdjList*) 
       malloc(V * sizeof(struct AdjList));
  
    for (int i = 0; i < V; ++i)
        graph->array[i].head = NULL;
  
    return graph;
}
  
void addEdge(struct Graph* graph, int src, 
                   int dest, int weight)
{
    struct AdjListNode* newNode = 
            newAdjListNode(dest, weight);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
  
    newNode = newAdjListNode(src, weight);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}
  
struct MinHeapNode
{
    int  v;
    int dist;
};
  
struct MinHeap
{  
    int size;     
    int capacity;  
    int *pos;    
    struct MinHeapNode **array;
};
  
struct MinHeapNode* newMinHeapNode(int v, 
                                 int dist)
{
    struct MinHeapNode* minHeapNode =
           (struct MinHeapNode*) 
      malloc(sizeof(struct MinHeapNode));
    minHeapNode->v = v;
    minHeapNode->dist = dist;
    return minHeapNode;
}
  
struct MinHeap* createMinHeap(int capacity)
{
    struct MinHeap* minHeap =
         (struct MinHeap*) 
      malloc(sizeof(struct MinHeap));
    minHeap->pos = (int *)malloc(
            capacity * sizeof(int));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array =
         (struct MinHeapNode**) 
                 malloc(capacity * 
       sizeof(struct MinHeapNode*));
    return minHeap;
}
  
void swapMinHeapNode(struct MinHeapNode** a, 
                     struct MinHeapNode** b)
{
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
  

void minHeapify(struct MinHeap* minHeap, 
                                  int idx)
{
    int smallest, left, right;
    smallest = idx;
    left = 2 * idx + 1;
    right = 2 * idx + 2;
  
    if (left < minHeap->size &&
        minHeap->array[left]->dist < 
         minHeap->array[smallest]->dist )
      smallest = left;
  
    if (right < minHeap->size &&
        minHeap->array[right]->dist <
         minHeap->array[smallest]->dist )
      smallest = right;
  
    if (smallest != idx)
    {
        MinHeapNode *smallestNode = 
             minHeap->array[smallest];
        MinHeapNode *idxNode = 
                 minHeap->array[idx];
  
        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v] = smallest;
  
        swapMinHeapNode(&minHeap->array[smallest], 
                         &minHeap->array[idx]);
  
        minHeapify(minHeap, smallest);
    }
}
  
int isEmpty(struct MinHeap* minHeap)
{
    return minHeap->size == 0;
}
  
struct MinHeapNode* extractMin(struct MinHeap* 
                                   minHeap)
{
    if (isEmpty(minHeap))
        return NULL;
  
    struct MinHeapNode* root = 
                   minHeap->array[0];
  
    struct MinHeapNode* lastNode = 
         minHeap->array[minHeap->size - 1];
    minHeap->array[0] = lastNode;
  
    minHeap->pos[root->v] = minHeap->size-1;
    minHeap->pos[lastNode->v] = 0;
  
    --minHeap->size;
    minHeapify(minHeap, 0);
  
    return root;
}
  
void decreaseKey(struct MinHeap* minHeap, 
                         int v, int dist)
{
    int i = minHeap->pos[v];
  
    minHeap->array[i]->dist = dist;
  
    while (i && minHeap->array[i]->dist < 
           minHeap->array[(i - 1) / 2]->dist)
    {
        minHeap->pos[minHeap->array[i]->v] = 
                                      (i-1)/2;
        minHeap->pos[minHeap->array[
                             (i-1)/2]->v] = i;
        swapMinHeapNode(&minHeap->array[i],  
                 &minHeap->array[(i - 1) / 2]);
  
        i = (i - 1) / 2;
    }
}
  
bool isInMinHeap(struct MinHeap *minHeap, int v)
{
   if (minHeap->pos[v] < minHeap->size)
     return true;
   return false;
}

void printPathList(int parent[], int j)
{
    if (parent[j] == -1)
        return;
    printPathList(parent, parent[j]);
    cout << j << " ";
}
  
void printArr(int dist[], int n, int parent[])
{
    int src = 0;

    printf("Vertex   Distance from Source\tPath");
    for (int i = 1; i < n; i++){
        printf("\n%d -> %d \t\t %d\t\t%d ", src, i, dist[i],
               src);
        printPathList(parent, i);
    }
}
  
void dijkstraList(struct Graph* graph, int src)
{
      
    int V = graph->V;
    
    int dist[V];

    int parent[V] = {-1};     
  
    struct MinHeap* minHeap = createMinHeap(V);
  
    for (int v = 0; v < V; ++v)
    {
        dist[v] = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, 
                                      dist[v]);
        minHeap->pos[v] = v;
    }
  
    minHeap->array[src] = 
          newMinHeapNode(src, dist[src]);
    minHeap->pos[src]   = src;
    dist[src] = 0;
    decreaseKey(minHeap, src, dist[src]);
  
    minHeap->size = V;
  
    while (!isEmpty(minHeap))
    {
        struct MinHeapNode* minHeapNode = 
                     extractMin(minHeap);
        int u = minHeapNode->v; 
  
        struct AdjListNode* pCrawl =
                     graph->array[u].head;
        while (pCrawl != NULL)
        {
            int v = pCrawl->dest;
  
            if (isInMinHeap(minHeap, v) && 
                      dist[u] != INT_MAX && 
              pCrawl->weight + dist[u] < dist[v])
            {
                parent[v] = u;
                dist[v] = dist[u] + pCrawl->weight;
  
                decreaseKey(minHeap, v, dist[v]);
            }
            pCrawl = pCrawl->next;
        }
    }
  
    printArr(dist, V,parent);
}
  
   
int minDistance(int dist[], bool sptSet[], int vertices)
{
    int min = INT_MAX, min_index;
    for (int i = 0; i < vertices; i++)
        if (sptSet[i] == false && dist[i] <= min)
            min = dist[i], min_index = i;
    return min_index;
}
 
void printPath(int parent[], int j)
{

    if (parent[j] == -1)
        return;
    printPath(parent, parent[j]);
    cout << j << " ";
}
 
void printSolution(int dist[], int parent[], int vertices)
{
    int src = 0;
    cout << "\nVertex\t Distance\tPath";
    for (int i = 1; i < vertices; i++) {
        printf("\n%d -> %d \t\t %d\t\t%d ", src, i, dist[i],src);
        printPath(parent, i);
    }
}
 
void dijkstraMatrix(int **graph, int vertices, int src)
{
    int dist[vertices];
 
    bool sptSet[vertices] = { false };
 
    int parent[vertices] = { -1 };
 
    for (int i = 0; i < vertices; i++)
        dist[i] = INT_MAX;
 
    dist[src] = 0;
 
    for (int count = 0; count < vertices - 1; count++) {
        int u = minDistance(dist, sptSet,vertices);
        sptSet[u] = true;
        for (int v = 0; v < vertices; v++)
            if (!sptSet[v] && graph[u][v]
                && dist[u] + graph[u][v] < dist[v]) {
                parent[v] = u;
                dist[v] = dist[u] + graph[u][v];
            }
    }
    printSolution(dist, parent, vertices);
}



void generate_graph_adj_matrix(int density, int vertices, int **graph){

    int remaining_numbers_to_fill = (int) density * vertices * vertices / 100;
    int random_weigh = 1;

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0,1);


    while(remaining_numbers_to_fill > 0){
        for(int i = 0; i < vertices; i++){
            for(int j = 0; j < vertices; j++){
               if(remaining_numbers_to_fill == 0) break;
               if(graph[i][j] == 0 && dis(gen) < 0.1){
                   graph[i][j] = rand() % 10 + 1;
                   remaining_numbers_to_fill--;
               }
               
               if(remaining_numbers_to_fill == 0) break;
            
            }
        }
    }
}

int ** initialize_graph(int vertices){
    int **graph = new int*[vertices];
    for(int i = 0; i < vertices; i++){
        graph[i] = new int[vertices];
    }
    
    for(int i = 0; i < vertices; i++){
        for(int j = 0; j < vertices; j++){
            graph[i][j] = 0;
        }
    }
    return graph;
}
 void print_graph_matrix(int vertices, int **graph){

    std::cout << "\n";
    for(int i = 0; i < vertices; i++){
        for(int j = 0; j < vertices; j++){
            cout << graph[i][j] << " ";
        }
    std::cout << '\n';
    }
 }
 
int ** load_graph_from_file_adj_matrix(){

    int **graph;
    string line;
    int start = 0;
    int end = 0;
    int weight = 0;

    ifstream graphFile("graphExample.txt");

    if(graphFile.is_open()){
        graphFile >> start >> end >> weight;
        graph = initialize_graph(end);
        while(graphFile >> start >> end >> weight){
            graph[start][end] = weight;
        }
        graphFile.close();
    }else{
        cout << "Nie mozna otworzyc pliku!";
        exit(0);
    }

    return graph;


}

void generate_graph_adj_list(int density, int vertices, Graph * graph){

    int remaining_numbers_to_fill = (int) density * vertices * vertices / 100;
    //std::cout << remaining_numbers_to_fill << "\n";
    int random_weigh = 1;

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0,1);


    while(remaining_numbers_to_fill > 0){
        for(int i = 0; i < vertices; i++){
            for(int j = 0; j < vertices; j++){
               if(remaining_numbers_to_fill == 0) break;
               if(dis(gen) < 0.1){
                   addEdge(graph,i,j, rand() % 10 + 1);
                   remaining_numbers_to_fill--;
                   //std::cout << remaining_numbers_to_fill << "\n";
               }
               
               if(remaining_numbers_to_fill == 0) break;
            
            }
        }
    }
} 

Graph* initialize_graph_list(int vertices){
    return createGraph(vertices);
}

Graph* load_graph_from_file_adj_list(){

    int start = 0;
    int end = 0;
    int weight = 0;
    struct Graph* graph;

    ifstream graphFile("graphExample.txt");

    if(graphFile.is_open()){
        graphFile >> start >> end >> weight;
        graph = createGraph(end);
        while(graphFile >> start >> end >> weight){
            addEdge(graph,start,end,weight);
        }
        graphFile.close();
    }else{
        cout << "Nie mozna otworzyc pliku!";
        exit(0);
    }

    return graph;


}


int main()
{
  
    //int **graph_from_file;
    //int **test = initialize_graph(vertices);
    //struct Graph* test = createGraph(vertices);
    //generate_graph_adj_list(10,vertices,test);
     //int **graph_from_file = load_graph_from_file_adj_matrix();
    //struct Graph* graph_from_file = load_graph_from_file_adj_list();
    //print_graph(5,graph_from_file);
    //dijkstraList(graph_from_file,0);
    //dijkstraMatrix(graph_from_file,5,0);

     int vertices = 12;
     int density = 100;
     freopen("test.txt","w",stdout);
	 
	auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < 100; i++){
        int **temp = initialize_graph(vertices);
        generate_graph_adj_matrix(density,vertices,temp);
        dijkstraMatrix(temp,vertices,0);
        delete [] temp;
    }
	auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	cout << "\n Czas wykonania: " << duration.count() << endl; 
	
	
	/*
	auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < 100; i++){
        struct Graph* temp= initialize_graph_list(vertices);
        generate_graph_adj_list(density,vertices,temp);
        dijkstraList(temp,0);
        delete [] temp;
    }
	auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	cout << "\n Czas wykonania: " << duration.count() << endl; */


    return 0;
}